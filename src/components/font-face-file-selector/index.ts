import './style.scss'

interface WPMediaLibrary {
	on: Function,
	open: Function,
	state: Function,
}

interface RefsType {
	button: HTMLElement,
	input: HTMLInputElement,
	mediaLibrary: WPMediaLibrary,
}

interface AttachmentAttributesType {
	id: number,
	filename: string,
}
interface AttachmentType {
	attributes: AttachmentAttributesType,
	id: number,
	fetch: Function,
}
interface StateType {
	attachment?: AttachmentType,
}

interface PropsType {
	onChange: Function
	initialText?: string
}

export default class FontFaceFileSelector {
	el: HTMLElement
	props: PropsType
	refs: RefsType
	state: StateType

	constructor(el:HTMLElement, props: PropsType ) {
		this.el = el
		this.props = props
		this.state = {}
		this.refs = {
			button: this.el.querySelector('button'),
			input: this.el.querySelector('input'),
			mediaLibrary: this.initMediaLibrary(),
		}

		if ( this.refs.input.value !== '' ) {
			this.retrieveAttachment( parseInt(this.refs.input.value) )
		}

		this.props.initialText = this.refs.button.textContent

		this.refs.button.addEventListener('click', this.onButtonClick.bind(this))
	}

	initMediaLibrary() {
		// @ts-ignore
		const mediaLibrary: WPMediaLibrary = wp.media({
			frame: 'select',
			title: this.props.initialText,
			multiple : false,
			// library : {
			// 	type : 'font/woff', // TODO find why this doens't work
			// }
		})

		mediaLibrary.on('open', this.onMediaLibraryOpen.bind(this))
		mediaLibrary.on('close', this.onMediaLibraryClose.bind(this))

		return mediaLibrary;
	}

	retrieveAttachment(id: number) {
		// @ts-ignore
		this.state.attachment = wp.media.attachment( id );
		this.state.attachment
			.fetch()
			.done(() => this.updateDOM())
	}

	updateDOM() {
		this.refs.button.textContent = (this.state.attachment?.attributes?.filename
			? this.state.attachment.attributes.filename
			: this.props.initialText
		)
	}

	onButtonClick() {
		this.refs.mediaLibrary.open()
	}

	// On open, get the id from the hidden input
	// and select the appropiate images in the media manager
	onMediaLibraryOpen() {
		this.refs.mediaLibrary.state().get('selection').add( this.state?.attachment ? [ this.state.attachment ] : [] );
	}

	onMediaLibraryClose() {
		var selection =  this.refs.mediaLibrary.state().get('selection');
		if ( selection.length > 0 ) {
			const attachment = selection.models[0]
			this.state = { attachment }
		}
		else {
			this.state = {}
		}
		this.refs.input.value = (this.state?.attachment?.id?.toString() ?? '')
		this.updateDOM()
		this.props.onChange();
	}
}
