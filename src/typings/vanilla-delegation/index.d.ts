interface HTMLElementAlt extends HTMLElement {
	addDelegateListener(eventType: string, selector: string, handler: Function, useCapture?: boolean): void
}

interface HTMLFormElementAlt extends HTMLFormElement {
	addDelegateListener(eventType: string, selector: string, handler: Function, useCapture?: boolean): void
}
