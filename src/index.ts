import "./style.scss"

import 'vanilla-delegation'

import CustomizerFontFaceControl from './components/customizer-font-face-control'
const SELECTOR = '.supt-cffc'

const COMPONENTS: Array<CustomizerFontFaceControl> = [];

// @ts-ignore
wp.customize.bind('ready', function () {
	const elements = Array.from(document.querySelectorAll(SELECTOR))
	elements.forEach((el: HTMLElement) => {
		COMPONENTS.push(new CustomizerFontFaceControl(el))
 	} )
})

